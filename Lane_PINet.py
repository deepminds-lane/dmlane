#############################################################################################################
##
##  Source code for testing
##
#############################################################################################################

import cv2
import json
import torch
import agent as agent
import numpy as np
from copy import deepcopy
#from data_loader import Generator
import time
from parameters import Parameters
import util as util
from tqdm import tqdm
import csaps
import os
from absl import app, flags, logging
from absl.flags import FLAGS

flags.DEFINE_string('video', './data/video/test.mp4', 'path to input video or set to 0 for webcam')
flags.DEFINE_string('output', None, 'path to output video')
flags.DEFINE_boolean('dont_show', False, 'dont show video output')


p = Parameters()
##############################
## Get agent and model
##############################
#print('Get agent')
lane_agent = agent.Agent()
# lane_agent.load_weights(804, "tensor(0.5786)")
lane_agent.load_weights(32, "tensor(1.1001)")
#lane_agent.load_weights(296, "tensor(1.6947)")
##############################
## Check GPU
##############################
#print('Setup GPU mode')
if torch.cuda.is_available():
  lane_agent.cuda()
  print('run in gpu')
##############################
## testing
##############################
#print('Testing loop')
lane_agent.evaluate_mode()


###############################################################
##
## Training
## 
###############################################################
def main(_argv):

    video_path = FLAGS.video

    # begin video capture
    try:
        cap = cv2.VideoCapture(int(video_path))
    except:
        cap = cv2.VideoCapture(video_path)
    #video_path = "/content/drive/MyDrive/vid/test-tf.mp4"
    #cap = cv2.VideoCapture(video_path)
    while(True):
        ret, frame = cap.read()
        # If the frame was not retrieved
        if not ret:
          print('>>> End')
          break      
        prevTime = time.time() 
#
        w_ratio = p.x_size * 1.0 / frame.shape[1]
        h_ratio = p.y_size* 1.0 / frame.shape[0]
        frame1 = cv2.resize(frame, (512,256))/255.0   
        frame1 = np.rollaxis(frame1, axis=2, start=0)       
        _, _, ti = test_ori(lane_agent, frame, np.array([frame1]), w_ratio, h_ratio)
        curTime = time.time()
        sec = curTime - prevTime
        fps = 1/(sec)
        s = "FPS : "+ str(fps)
        print(s)
        ti[0] = cv2.resize(ti[0], (1280,800))
        ti[0] = cv2.resize(ti[0], (w,h))
        
        if not FLAGS.dont_show:
            cv2.imshow("Output Video", ti[0])
            
        # Initializing writer only once        
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')  
        # Writing current processed frame into the video file
        writer = cv2.VideoWriter('result.mp4', fourcc, 25,
                                 (frame.shape[1], frame.shape[0]), True)
        # Write processed current frame to the file
        writer.write(ti[0])

    # Releasing video reader and writer
    cap.release()
    writer.release()
    
    
def Lane(frame):
    w_ratio = p.x_size * 1.0 / frame.shape[1]
    h_ratio = p.y_size* 1.0 / frame.shape[0]
    frame1 = cv2.resize(frame, (512,256))/255.0   
    frame1 = np.rollaxis(frame1, axis=2, start=0)       
    _, _, ti = test_ori(lane_agent, frame, np.array([frame1]), w_ratio, h_ratio)
    result = cv2.addWeighted(ti[0],.78,horizontal(frame),.22,0)

    return result


# draws a horizontal line on a frame
def horizontal(img):
    def pts_rectangle(pts, theta_):
    	dict_relation = {th_: pt for pt, th_ in zip(pts, theta_)}
    	avg = lambda x1: sum(x1) / len(x1) if len(x1) != 0 else 0
    	th = round(avg(theta_))
    	counter = 0
    	while True:
        	counter += 1
        	for coefficient in [0, -1, 1]:
	            try:
        	        return dict_relation[th + counter * coefficient]
	            except KeyError:
        	        pass

    img2 = img.copy()
    h, w = img2.shape[:2]
    p1 = (round(.33 * w), round(.71 * h))
    p2 = (round(.98 * w), round(.92 * h))
    img = img[p1[1]:p2[1], p1[0]:p2[0]]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imwrite('gray.jpg',gray)
    edges = cv2.Canny(gray, 300, 10)
    cv2.imwrite('edges.jpg',edges)
    lines_ = []
    for theta in range(85, 96):
        lines_.append(cv2.HoughLinesP(edges, 1, theta * np.pi / 180, 125, None, 50, 50))
    pts = []
    theta_ = []
    for lines, theta in zip(lines_, range(85, 96)):
        try:
            for line in lines[0]:
                pt1 = (line[0], line[1])
                pt2 = (line[2], line[3])
                if pt1[0] == pt2[0]:
                    continue
                if abs((pt1[1] - pt2[1]) / (pt1[0] - pt2[0])) > .2:
                    continue
                pts.append([pt1, pt2])
                theta_.append(theta)
        except TypeError:
            pass

    if len(pts) >= 4:
        pt1, pt2 = pts_rectangle(pts, theta_)
        for u in range(-4, 5):
            m = (pt1[1] - pt2[1]) / (pt1[0] - pt2[0])
            b = pt1[1] - m * pt1[0]
            x0 = pt2[0] - round(.15 * (p2[0] - p1[0]))
            y0 = round(m * x0 + b)
            x3 = pt2[0] - round(.75 * (p2[0] - p1[0]))
            y3 = round(m * x3 + b)
            cv2.line(img, (x3, int(y3 + u)), (x0, int(y0 + u)), (255, 0, 0), 3)

    img2[p1[1]:p2[1], p1[0]:p2[0]] = img
    return img2
############################################################################
## evaluate on the test dataset
############################################################################
#def evaluation(loader, lane_agent, index= -1, thresh = p.threshold_point, name = None):
#    result_data = deepcopy(loader.test_data)
#    progressbar = tqdm(range(loader.size_test//4))
#    for test_image, target_h, ratio_w, ratio_h, testset_index, gt in loader.Generate_Test():
#        x, y, _ = test(lane_agent, test_image, thresh, index)
#        x_ = []
#        y_ = []
#        for i, j in zip(x, y):
#            temp_x, temp_y = util.convert_to_original_size(i, j, ratio_w, ratio_h)
#            x_.append(temp_x)
#            y_.append(temp_y)
#        #x_, y_ = find_target(x_, y_, target_h, ratio_w, ratio_h)
#        x_, y_ = fitting(x_, y_, target_h, ratio_w, ratio_h)
#        result_data = write_result_json(result_data, x_, y_, testset_index)
#
#        #util.visualize_points_origin_size(x_[0], y_[0], test_image[0], ratio_w, ratio_h)
#        #print(gt.shape)
#        #util.visualize_points_origin_size(gt[0], y_[0], test_image[0], ratio_w, ratio_h)
#
#        progressbar.update(1)
#    progressbar.close()
#    if name == None:
#        save_result(result_data, "test_result.json")
#    else:
#        save_result(result_data, name)

############################################################################
## linear interpolation for fixed y value on the test dataset, if you want to use python2, use this code
############################################################################
def find_target(x, y, target_h, ratio_w, ratio_h):
    # find exact points on target_h
    out_x = []
    out_y = []
    x_size = p.x_size/ratio_w
    y_size = p.y_size/ratio_h
    count = 0
    for x_batch, y_batch in zip(x,y):
        predict_x_batch = []
        predict_y_batch = []
        for i, j in zip(x_batch, y_batch):
            min_y = min(j)
            max_y = max(j)
            temp_x = []
            temp_y = []
            for h in target_h[count]:
                temp_y.append(h)
                if h < min_y:
                    temp_x.append(-2)
                elif min_y <= h and h <= max_y:
                    for k in range(len(j)-1):
                        if j[k] >= h and h >= j[k+1]:
                            #linear regression
                            if i[k] < i[k+1]:
                                temp_x.append(int(i[k+1] - float(abs(j[k+1] - h))*abs(i[k+1]-i[k])/abs(j[k+1]+0.0001 - j[k])))
                            else:
                                temp_x.append(int(i[k+1] + float(abs(j[k+1] - h))*abs(i[k+1]-i[k])/abs(j[k+1]+0.0001 - j[k])))
                            break
                else:
                    if i[0] < i[1]:
                        l = int(i[1] - float(-j[1] + h)*abs(i[1]-i[0])/abs(j[1]+0.0001 - j[0]))
                        if l > x_size or l < 0 :
                            temp_x.append(-2)
                        else:
                            temp_x.append(l)
                    else:
                        l = int(i[1] + float(-j[1] + h)*abs(i[1]-i[0])/abs(j[1]+0.0001 - j[0]))
                        if l > x_size or l < 0 :
                            temp_x.append(-2)
                        else:
                            temp_x.append(l)
            predict_x_batch.append(temp_x)
            predict_y_batch.append(temp_y)                            
        out_x.append(predict_x_batch)
        out_y.append(predict_y_batch)
        count += 1
    
    return out_x, out_y

def fitting(x, y, target_h, ratio_w, ratio_h):
    out_x = []
    out_y = []
    count = 0
    x_size = p.x_size/ratio_w
    y_size = p.y_size/ratio_h

    for x_batch, y_batch in zip(x,y):
        predict_x_batch = []
        predict_y_batch = []
        for i, j in zip(x_batch, y_batch):
            min_y = min(j)
            max_y = max(j)
            temp_x = []
            temp_y = []

            jj = []
            pre = -100
            for temp in j[::-1]:
                if temp > pre:
                    jj.append(temp)
                    pre = temp
                else:
                    jj.append(pre+0.00001)
                    pre = pre+0.00001
            sp = csaps.CubicSmoothingSpline(jj, i[::-1], smooth=0.0001)

            last = 0
            last_second = 0
            last_y = 0
            last_second_y = 0
            for h in target_h[count]:
                temp_y.append(h)
                if h < min_y:
                    temp_x.append(-2)
                elif min_y <= h and h <= max_y:
                    temp_x.append( sp([h])[0] )
                    last = temp_x[-1]
                    last_y = temp_y[-1]
                    if len(temp_x)<2:
                        last_second = temp_x[-1]
                        last_second_y = temp_y[-1]
                    else:
                        last_second = temp_x[-2]
                        last_second_y = temp_y[-2]
                else:
                    if last < last_second:
                        l = int(last_second - float(-last_second_y + h)*abs(last_second-last)/abs(last_second_y+0.0001 - last_y))
                        if l > x_size or l < 0 :
                            temp_x.append(-2)
                        else:
                            temp_x.append(l)
                    else:
                        l = int(last_second + float(-last_second_y + h)*abs(last_second-last)/abs(last_second_y+0.0001 - last_y))
                        if l > x_size or l < 0 :
                            temp_x.append(-2)
                        else:
                            temp_x.append(l)
            predict_x_batch.append(temp_x)
            predict_y_batch.append(temp_y)
        out_x.append(predict_x_batch)
        out_y.append(predict_y_batch) 
        count += 1

    return out_x, out_y

############################################################################
## write result
############################################################################
def write_result_json(result_data, x, y, testset_index):
    for index, batch_idx in enumerate(testset_index):
        for i in x[index]:
            result_data[batch_idx]['lanes'].append(i)
            result_data[batch_idx]['run_time'] = 1
    return result_data

############################################################################
## save result by json form
############################################################################
def save_result(result_data, fname):
    with open(fname, 'w') as make_file:
        for i in result_data:
            json.dump(i, make_file, separators=(',', ': '))
            make_file.write("\n")

############################################################################
## test on the input test image
############################################################################
def test(lane_agent, test_images, thresh = p.threshold_point, index= -1):

    result = lane_agent.predict_lanes_test(test_images)
    torch.cuda.synchronize()
    confidences, offsets, instances = result[index]
    
    num_batch = len(test_images)

    out_x = []
    out_y = []
    out_images = []
    
    for i in range(num_batch):
        # test on test data set
        image = deepcopy(test_images[i])
        image = np.rollaxis(image, axis=2, start=0)
        image = np.rollaxis(image, axis=2, start=0)*255.0
        image = image.astype(np.uint8).copy()

        confidence = confidences[i].view(p.grid_y, p.grid_x).cpu().data.numpy()

        offset = offsets[i].cpu().data.numpy()
        offset = np.rollaxis(offset, axis=2, start=0)
        offset = np.rollaxis(offset, axis=2, start=0)
        
        instance = instances[i].cpu().data.numpy()
        instance = np.rollaxis(instance, axis=2, start=0)
        instance = np.rollaxis(instance, axis=2, start=0)

        # generate point and cluster
        raw_x, raw_y = generate_result(confidence, offset, instance, thresh)

        # eliminate fewer points
        in_x, in_y = eliminate_fewer_points(raw_x, raw_y)
                
        # sort points along y 
        in_x, in_y = util.sort_along_y(in_x, in_y)  

        result_image = util.draw_points(in_x, in_y, deepcopy(image))

        out_x.append(in_x)
        out_y.append(in_y)
        out_images.append(result_image)
        
    return out_x, out_y,  out_images

############################################################################
## test on the input test image,and show result on original output.
############################################################################
def test_ori(lane_agent, ori_image, test_images, w_ratio, h_ratio, thresh=p.threshold_point):  # p.threshold_point:0.81

    result = lane_agent.predict_lanes_test(test_images)
    torch.cuda.synchronize()
    confidences, offsets, instances = result[-1]
    
    num_batch = len(test_images)

    out_x = []
    out_y = []
    out_images = []
    
    for i in range(num_batch):
        # test on test data set
        image = deepcopy(test_images[i])
        image = np.rollaxis(image, axis=2, start=0)
        image = np.rollaxis(image, axis=2, start=0)*255.0
        image = image.astype(np.uint8).copy()

        confidence = confidences[i].view(p.grid_y, p.grid_x).cpu().data.numpy()
 
        offset = offsets[i].cpu().data.numpy()
        offset = np.rollaxis(offset, axis=2, start=0)
        offset = np.rollaxis(offset, axis=2, start=0)
        
        instance = instances[i].cpu().data.numpy()
        instance = np.rollaxis(instance, axis=2, start=0)
        instance = np.rollaxis(instance, axis=2, start=0)
   
        # generate point and cluster
        raw_x, raw_y = generate_result(confidence, offset, instance, thresh)

        # eliminate fewer points
        in_x, in_y = eliminate_fewer_points(raw_x, raw_y)
     
        # sort points along y 
        in_x, in_y = util.sort_along_y(in_x, in_y)  

        
        result_image = util.draw_point_ori(in_x, in_y, ori_image, w_ratio, h_ratio)  # 将最后且后处理后的坐标点绘制与原图上.
        
        out_x.append(in_x)
        out_y.append(in_y)
        out_images.append(result_image)
        
    return out_x, out_y,  out_images

############################################################################
## eliminate result that has fewer points than threshold
############################################################################
def eliminate_fewer_points(x, y):
    # eliminate fewer points
    out_x = []
    out_y = []
    for i, j in zip(x, y):
        if len(i)>2:
            out_x.append(i)
            out_y.append(j)     
    return out_x, out_y   

############################################################################
## generate raw output
############################################################################
def generate_result(confidance, offsets,instance, thresh):

    mask = confidance > thresh

    grid = p.grid_location[mask]

    offset = offsets[mask]
    feature = instance[mask]

    lane_feature = []
    x = []
    y = []
    for i in range(len(grid)):
        if (np.sum(feature[i]**2))>=0:
            point_x = int((offset[i][0]+grid[i][0])*p.resize_ratio)
            point_y = int((offset[i][1]+grid[i][1])*p.resize_ratio)
            if point_x > p.x_size or point_x < 0 or point_y > p.y_size or point_y < 0:
                continue
            if len(lane_feature) == 0:
                lane_feature.append(feature[i])
                x.append([point_x])
                y.append([point_y])
            else:
                flag = 0
                index = 0
                min_feature_index = -1
                min_feature_dis = 10000
                for feature_idx, j in enumerate(lane_feature):
                    dis = np.linalg.norm((feature[i] - j)**2)
                    if min_feature_dis > dis:
                        min_feature_dis = dis
                        min_feature_index = feature_idx
                if min_feature_dis <= p.threshold_instance:
                    lane_feature[min_feature_index] = (lane_feature[min_feature_index]*len(x[min_feature_index]) + feature[i])/(len(x[min_feature_index])+1)
                    x[min_feature_index].append(point_x)
                    y[min_feature_index].append(point_y)
                elif len(lane_feature) < 12:
                    lane_feature.append(feature[i])
                    x.append([point_x])
                    y.append([point_y])
                
    return x, y

if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass